# k8s-test-app

A simple test application for kubernetes. The application is written in nodeJS and
can be used to demonstrate how a node application can be containerized and run in a
kubernetes cluster

## Quick start kubernetes
Prereq helm installed in kubernetes cluster.

`helm install --name <name> test-app-chart`

## Server
the server is run by using the command `node server.js`. The server will start and connect to a
mongoDB database. The location of the database is set with environment variable DB_DNS.
The dockerfile can be located at dockerhub.com/vanneback/test-app-backend

## Client
The client can be quick started by using `npm start`. In production it is deployed with 
`npm run build` and then it is run with as a nginx server. The production is built to
run in a kubernetes cluster and the backend dns is set by setting the environment variables
`BACKEND_SERVICE` and `BACKEND_NAMESPACE`. The server is then run by running the script 
in client/template.
The dockerfile can be located at dockerhub.com/vanneback/test-app-frontend

## Database
The database uses mongoDB at port 27017. The database can be started locally in a dockercontainer
with `docker run --rm -p 27017:27017 mongo`